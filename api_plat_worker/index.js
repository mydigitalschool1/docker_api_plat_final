const express = require('express')
const redis = require('redis')
const amqp = require('amqplib');
const cors = require('cors');

const app = express()
const client = redis.createClient({
    host: 'redis-server',
    port: 6379
})

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.use(cors({
  origin: 'http://localhost:4002',
'methods': 'GET,HEAD,PUT,PATCH,POST,DELETE',
}));

//Set initial plats
client.set('plats', 0);


//defining the root endpoint
app.get('/', (req, res) => {
    client.get('plats', (err, plats) => {
        res.send('Number of plats is: ' + plats)
        //client.set('plats', parseInt(plats) + 1)
    })
})

//add 1 plat at each visit
app.get('/api/plats/add', (req, res) => {
    client.get('plats', (err, plats) => {
        //res.send('Number of plats is: ' + plats)
        var data = {"plat": plats}
        res.send(JSON.stringify(data))
        client.set('plats', parseInt(plats) + 1)
    })
})

//created to debug => create a queue
  amqp.connect('amqp://rabbitmq:5672')
  .then(conn => {
    return conn.createChannel();
  })
  .then(ch => {
    const queue = 'myQueue';
    const options = { durable: true };
    return ch.assertQueue(queue, options);
  })
  .then(() => {
    console.log('Queue declared with durable flag');
  })
  .catch(console.warn);

// #region sendFlag
app.get('/api/sendFlag', function (req, res) {
  res.header("Content-Type",'application/json');
  createViewFlag(res)
})

function createViewFlag(res){
  console.log('createFlag');
    amqp.connect('amqp://rabbitmq:5672')
  .then(conn => {
    return conn.createChannel();
  })
  .then(ch => {
    const queue = 'addPlat';
    const message = 'request';
    const options = { persistent: true, durable: true };
    //const options = { Durable: Transient };
    return ch.assertQueue(queue, options);
    
  })
  .then(() =>{
    sendMessage()
  })
  .then(() => {
    console.log("flag sent !")
    var data = {"flag": "sent"}
    res.send(JSON.stringify(data));
    //return true;
  })
  .catch(console.warn);
}

function sendMessage(){
  amqp.connect('amqp://rabbitmq:5672')
  .then(conn => {
    return conn.createChannel();
  })
  .then(ch => {
    const queue = 'addPlat';
    const message = 'request';
    const options = { persistent: true };
    return ch.sendToQueue(queue, Buffer.from(message), options);
  })
  .then(() => {
    console.log('Message sent with persistent flag');
  })
  .catch(console.warn);
}
// #endregion

// #region receiveFlag
app.get('/api/receiveFlag', function (req, res) {
  res.header("Content-Type",'application/json');
  //res.send(JSON.stringify(plats));
  amqp.connect('amqp://rabbitmq:5672')
  .then(conn => {
    return conn.createChannel();
  })
  .then(ch => {
    const queue = 'addPlat';
    return ch.consume(queue, (msg) => {
        if(msg == "request"){
            //Change the flag message
            createFlag(res, plats);
        }
        console.log('Received message:', msg.content.toString());
    });
  })
  .then(() => {
    console.log('Consumer created');
  })
  .catch(console.warn);
})

function createFlag(res, pl){
    amqp.connect('amqp://rabbitmq:5672')
  .then(conn => {
    return conn.createChannel();
  })
  .then(ch => {
    const queue = 'addPlat';
    const message = pl.toString();
    const options = { persistent: true, durable: true };
    return ch.sendToQueue(queue, Buffer.from(message), options);
  })
  .then(() => {
    console.log('Message sent with flag');
    var data = {"flag": message}
    res.send(JSON.stringify(data));
  })
  .catch(console.warn);
}
// #endregion

//specifying the listening port
app.listen(4001, ()=>{
    console.log('Listening on port 4001')
})

module.exports = app;