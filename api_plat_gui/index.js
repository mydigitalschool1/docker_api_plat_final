const express = require('express')
const amqp = require('amqplib');

const app = express()
var router = express.Router()

var path = __dirname + '/views/';

router.use(function (req,res,next) {
    console.log("/" + req.method);
    next();
  });

app.use(express.static(path));
app.use("/", router);

//specifying the listening port
app.listen(4002, ()=>{
    console.log('Listening on port 4002')
})

module.exports = app;