# docker_api_plat_final

This project was made after an attempt - a full fail - with another api docker where i tried to create a full docker app; an Api with redis or mongodb in an other container and link it to the backend, but it didn't worked at all. So i made a basic api with redis that is functionnal but not ideal.
link: [my poor crash test](https://gitlab.com/mydigitalschool1/docker_api_plat_2023)

The project web views is at [localhost:4002](http://localhost:4002/)

The project api is at [localhost:4001](http://localhost:4001/)

Click on the button to add a plat.

## How to run it

To begin with, clone the project:
```
git clone <this-repo>
```

Then open all project, then go to `api_plat_worker` directory with a terminal :

```
cd .\api_plat_worker\
```

Then run: 

```
docker-compose up
```

Then **open an other** terminal:

```
cd .\api_plat_gui\
docker-compose up
```

## Routes (api documentation)

$\colorbox{blue}{{\color{white}{GET: /api/plats/add}}}{ \ : \ increase \  the \ number \ of \  "plats"}$

$\colorbox{blue}{{\color{white}{GET: /api/sendFlag}}}{ \ : \ create \  an \ "addPlat" \ flag \ with \  "request" \ message}$

$\colorbox{blue}{{\color{white}{GET: /api/receiveFlag}}}{ \ : \ receive \  an \ "addPlat" \ flag \ and \  get \ last \ message}$