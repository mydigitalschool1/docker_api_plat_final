function fetchData(){
    fetch('http://localhost:4001/api/plats/add', {
    method: 'GET', // or 'PUT'
    headers: {
       'Content-Type': 'application/json',
    },
    //body: JSON.stringify(data),
    })
    .then((response) => response.json())
    .then((data) => {
       console.log('Success:', data);

       document.getElementById('nbPlats').innerText = JSON.stringify(data["plat"]);
       receiveFlag()
       
       //then update flag
       //updateFlag()
    })
    .catch((error) => {
       console.error('Error:', error);
    });
 }

 //if data have been changed in api then update th view
 function updateFlag(){
  fetch('http://localhost:4001/api/sendFlag', {
    method: 'GET', // or 'PUT'
    headers: {
       'Content-Type': 'application/json',
    },
    //body: JSON.stringify(data),
    })
    .then((response) => response.json())
    .then((data) => {
       console.log('Success:', data);       
       console.log('Success:', data["flag"]);       
       //then update flag
       if(data["flag"] == "sent"){
        fetchData()
       }
    })
    .catch((error) => {
       console.error('Error:', error);
    });
 }

 function receiveFlag(){
   fetch('http://localhost:4001/api/receiveFlag', {
     method: 'GET', // or 'PUT'
     headers: {
        'Content-Type': 'application/json',
     },
     //body: JSON.stringify(data),
     })
     .then((response) => response.json())
     .then((data) => {
        console.log('Success receive:', data);       
        //then update flag
        if(data){
         console.log("Flag successfully received")
        }
     })
     .catch((error) => {
        console.error('Error:', error);
     });
  }